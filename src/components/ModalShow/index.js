import React from "react";
import PokeModal from "react-modal";
import closeIcon from "~/assets/images/close.png";

import { overlayStyle, contentStyle } from "./modalStyles";

import Button from "~/components/Button";

import {
  Content,
  Avatar,
  Stats,
  Stat,
  Title,
  Type,
  Skills,
  Statistics,
  Statistic,
} from "./styles";

import Shield from "~/assets/images/shield.png";
import Speed from "~/assets/images/speed.png";
import Sword from "~/assets/images/sword.png";

PokeModal.setAppElement("#root");

const typesPT = {
  normal: "normal",
  fighting: "lutador",
  flying: "voador",
  poison: "venenoso",
  ground: "terra",
  rock: "pedra",
  bug: "inseto",
  ghost: "fantasma",
  steel: "metal",
  fire: "fogo",
  water: "água",
  grass: "planta",
  electric: "elétrico",
  psychic: "psíquico",
  ice: "gelo",
  dragon: "dragão",
  dark: "noturno",
  fairy: "Fada",
  unknown: "desconhecido",
  shadow: "sombra",
};

const statsPT = {
  attack: "ataque",
  defense: "defesa",
  special_attack: "ataque especial",
  special_defense: "defesa especial",
  speed: "velocidade",
};

export default function ModalShow({ openModal, closeModal, data, action }) {
  return (
    <PokeModal
      className="modalScrollBar"
      isOpen={openModal}
      onRequestClose={closeModal}
      style={{ overlay: overlayStyle, content: contentStyle }}>
      <button className="closeModal" type="button" onClick={closeModal}>
        <img className="modal__close" src={closeIcon} alt="Fechar" />
      </button>
      <Content>
        <Avatar>
          <img
            src={data?.sprites?.front_default || data?.sprites?.back_default}
            alt={data && data?.nome}
          />
        </Avatar>
        <h2>{data?.name}</h2>
        <Stats>
          <Stat>
            <h5>hp</h5>
            <h2>
              {data?.stats && data?.stats[0]?.base_stat}/
              {data?.stats && data?.stats[0]?.base_stat}
            </h2>
          </Stat>
          <div className="vl" />
          <Stat>
            <h5>altura</h5>
            <h2>{data?.height / 10} m</h2>
          </Stat>
          <div className="vl" />
          <Stat>
            <h5>peso</h5>
            <h2>{data?.weight / 10} kg</h2>
          </Stat>
        </Stats>
        <Title>
          <hr className="hl" />
          <h1>tipo</h1>
          <hr className="hl" />
        </Title>
        <Type>
          <span className="first-type">
            {data?.types && typesPT[data?.types[0]?.type?.name]}
          </span>
          {data?.types && data?.types[1] && (
            <span className="second-type">
              {data?.types && typesPT[data?.types[1]?.type?.name]}
            </span>
          )}
        </Type>
        <Title className="skills">
          <hr className="skills-hl" />
          <h1>habilidades</h1>
          <hr className="skills-hl" />
        </Title>
        <Skills>
          {data?.abilities &&
            data?.abilities.map((skill, index) => (
              <p key={skill?.slot}>
                {skill?.is_hidden === false &&
                  (index ? ", " : "") + skill?.ability?.name}
              </p>
            ))}
        </Skills>
        <Title className="statistics">
          <hr className="statistics-hl" />
          <h1>estatísticas</h1>
          <hr className="statistics-hl" />
        </Title>
        <Statistics>
          <Statistic>
            <span>
              <img src={Sword} alt="Ataque" />
              <p>{data?.stats && statsPT[data?.stats[1]?.stat?.name]}</p>
            </span>
            <p className="value">{data?.stats && data?.stats[1]?.base_stat}</p>
          </Statistic>
          <Statistic>
            <span>
              <img src={Shield} alt="Defesa" />
              <p>{data?.stats && statsPT[data?.stats[2]?.stat?.name]}</p>
            </span>
            <p className="value">{data?.stats && data?.stats[2]?.base_stat}</p>
          </Statistic>
          <Statistic>
            <span>
              <img src={Sword} alt="Ataque" />
              <p>ataque especial</p>
            </span>
            <p className="value">{data?.stats && data?.stats[3]?.base_stat}</p>
          </Statistic>
          <Statistic>
            <span>
              <img src={Shield} alt="Defesa" />
              <p>defesa especial</p>
            </span>
            <p className="value">{data?.stats && data?.stats[4]?.base_stat}</p>
          </Statistic>
          <Statistic>
            <span>
              <img src={Speed} alt="Velocidade" />
              <p>{data?.stats && statsPT[data?.stats[5]?.stat?.name]}</p>
            </span>
            <p className="value">{data?.stats && data?.stats[5]?.base_stat}</p>
          </Statistic>
        </Statistics>
        <Button onClick={action} text="LIBERAR POKEMON" />
      </Content>
    </PokeModal>
  );
}
