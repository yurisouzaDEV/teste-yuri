export const overlayStyle = {
  zIndex: 10,
  height: "100vh",
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  overflow: "hidden",
  outline: "none",
  backgroundColor: "rgba(0, 0, 0, 0.5)",
};

export const contentStyle = {
  display: "flex",
  flexDirection: "column",
  top: "0",
  left: "0",
  right: "0",
  bottom: "0",
  position: "relative",
  background: "linear-gradient(90deg, #43E97B 0%, #38F9D7 100%)",
  boxShadow:
    "0 0 1px rgba($color-black, 0.3), 0 4px 8px rgba($color-black, 0.25)",
  borderRadius: "8px",
  padding: "0",
  width: "100%",
  maxWidth: "360px",
  height: "560px",
  border: "none",
  overflow: "auto",
  overflowX: "hidden",
};
