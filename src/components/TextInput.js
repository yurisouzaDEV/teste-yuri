import React, { useEffect, useRef } from "react";
import { useField } from "@unform/core";

export default function TextInput({
  className,
  label,
  placeholder,
  name,
  ...rest
}) {
  const inputRef = useRef(null);
  const { fieldName, defaultValue, registerField, error } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: "value",
    });
  }, [fieldName, registerField]);

  return (
    <div className={`${className} input__container`}>
      {label && <label className="input__label">{label}</label>}
      <input
        className="input"
        type="text"
        placeholder={placeholder}
        id={fieldName}
        name={fieldName}
        ref={inputRef}
        defaultValue={defaultValue}
        {...rest}
      />
    </div>
  );
}
