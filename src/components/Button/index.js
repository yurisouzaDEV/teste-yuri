import React from "react";

export default function Button({ text, icon, onClick }) {
  return (
    <button onClick={onClick} className={`btn btn--${text ? "text" : "icon"}`}>
      {text || icon}
    </button>
  );
}
