import React from "react";
import chevron from "../assets/images/chevronDownBlack.png";

const typesPT = {
  normal: "normal",
  fighting: "lutador",
  flying: "voador",
  poison: "venenoso",
  ground: "terra",
  rock: "pedra",
  bug: "inseto",
  ghost: "fantasma",
  steel: "metal",
  fire: "fogo",
  water: "água",
  grass: "planta",
  electric: "elétrico",
  psychic: "psíquico",
  ice: "gelo",
  dragon: "dragão",
  dark: "noturno",
  fairy: "Fada",
  unknown: "desconhecido",
  shadow: "sombra",
};

const Dropdown = ({ options, multiple }) => {
  return (
    <div className="dropdown__container">
      <img src={chevron} className="dropdown__icon" alt="Chevron" />
      <select className="dropdown">
        <option className="dropdown__option" value="">
          Selecione o(s) tipo(s)
        </option>
        {options &&
          options.map((option, index) => (
            <option
              key={option?.value}
              className="dropdown__option"
              value={option.value}>
              {typesPT[option.name]}
            </option>
          ))}
      </select>
    </div>
  );
};

export default Dropdown;
