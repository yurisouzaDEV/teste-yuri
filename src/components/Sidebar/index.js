import React, { useState, useCallback } from "react";
import Button from "~/components/Button";
import plusIcon from "~/assets/images/plus.png";

import ModalShow from "~/components/ModalShow";
import ModalCreate from "~/components/ModalCreate";

export default function Sidebar({ pokes }) {
  const [openModal, setOpenModal] = useState(false);
  const [openModalCreate, setOpenModalCreate] = useState(false);
  const [selected, setSelected] = useState(null);

  const handleOpen = useCallback(() => {
    setOpenModal(true);
  }, []);

  const handleOpenModalCreate = useCallback(() => {
    setOpenModalCreate(true);
  }, []);

  const handleClose = useCallback(() => {
    setOpenModal(false);
    setOpenModalCreate(false);
  }, []);

  const handleShow = useCallback(
    (id) => {
      const filter = pokes.filter((stats) => stats.id === id);
      setSelected(filter[0]);
    },
    [pokes]
  );

  const handleDelete = useCallback(
    (id) => {
      const filter = pokes.findIndex((stats) => stats.id === id);

      pokes.splice(filter, 1);
      setSelected(filter[0]);
    },
    [pokes]
  );

  return (
    <>
      <div className="sidebar">
        {pokes &&
          pokes.map((i) => (
            <div className="sidebar__item" key={i.id}>
              <button
                type="button"
                onClick={() => {
                  handleOpen();
                  handleShow(i.id);
                }}>
                <img src={i?.sprites?.front_default} alt={i.name} />
              </button>
            </div>
          ))}
        {pokes.length < 6 && (
          <>
            <div className="sidebar__item">?</div>{" "}
            <Button
              icon={<img src={plusIcon} alt="+" />}
              onClick={() => handleOpenModalCreate()}
            />
          </>
        )}
      </div>
      <ModalShow
        data={selected}
        openModal={openModal}
        closeModal={handleClose}
        action={() => {
          handleDelete(selected.id);
          handleClose();
        }}
      />
      <ModalCreate openModal={openModalCreate} closeModal={handleClose} />
    </>
  );
}
