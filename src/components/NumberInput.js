import React, { useEffect, useRef } from "react";
import { useField } from "@unform/core";

import chevron from "../assets/images/chevronDownBlack.png";

export default function NumberInput({
  className,
  icon,
  label,
  placeholder,
  name,
  suffix,
  ...rest
}) {
  const inputRef = useRef(null);
  const { fieldName, defaultValue, registerField, error } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: "value",
    });
  }, [fieldName, registerField]);

  return (
    <div className={`${className} input__container`}>
      <span className="label-icons">
        {icon && <img src={icon} alt="icon" />}
        {label && <label className="input__label">{label}</label>}
      </span>
      <div className="input__number">
        <input
          className="input"
          type="number"
          placeholder={placeholder}
          id={fieldName}
          name={fieldName}
          ref={inputRef}
          defaultValue={defaultValue}
          {...rest}
        />
        {suffix && <p className="input__suffix">{suffix}</p>}
        <div className="input__btns">
          <img src={chevron} className="input__increase" alt="Mais" />
          <img src={chevron} className="input__decrease" alt="Menos" />
        </div>
      </div>
    </div>
  );
}
