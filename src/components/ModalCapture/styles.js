import styled from "styled-components";

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: #f7f9fc;
  border-radius: 24px 24px 0 0;
  top: 150px;
  position: relative;
  width: 100%;

  h2 {
    font-family: "OpenSansBold", sans-serif;
    font-size: 18px;
    line-height: 24px;
    color: #2e3a59;
    text-transform: uppercase;
    margin-top: 162px;
  }
`;

export const Avatar = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  top: -120px;
  background-color: #f7f9fc;
  border-radius: 50%;
  border: 4px solid #00d68f;
  width: 100%;
  max-width: 247px;

  img {
    width: 100%;
    border-radius: 50%;
  }
`;

export const Stats = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-top: 36px;
  width: 100%;
  padding: 0 40px;

  .vl {
    border-left: 1px solid #c5cee0;
    height: 48px;
  }
`;

export const Stat = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  max-width: 75px;

  h5 {
    font-family: "OpenSansBold", sans-serif;
    font-size: 12px;
    line-height: 16px;
    color: #2e3a59;
    text-transform: uppercase;
  }

  h2 {
    margin-top: 8px;
    text-transform: none;
  }
`;

export const Title = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 24px;
  padding: 0 24px;

  h1 {
    font-family: "OpenSansSemiBold", sans-serif;
    font-size: 15px;
    line-height: 24px;
    color: #2e3a59;
    text-transform: uppercase;
    margin: 0 12px;
  }

  .hl {
    border-bottom: 1px solid #c5cee0;
    width: 100vw;
    max-width: 125px;
  }

  &.skills {
    margin-top: 0;
  }

  .skills-hl {
    border-bottom: 1px solid #c5cee0;
    width: 100vw;
    max-width: 95px;
  }
`;

export const Type = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: 100%;
  max-width: 215px;
  margin: 30px 0 42px 0;

  span {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    font-family: "OpenSansSemiBold", sans-serif;
    font-size: 12px;
    line-height: 16px;
    text-transform: uppercase;
    color: #ffffff;
    width: 100%;
    max-width: 100px;
    padding: 8px 0;
    border-radius: 42px;
  }

  .first-type {
    background-color: #67af32;
  }

  .second-type {
    background-color: #924990;
    margin-left: 12px;
  }
`;

export const Skills = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  font-family: "OpenSansBold", sans-serif;
  font-size: 12px;
  line-height: 16px;
  text-transform: uppercase;
  margin: 38px 0 165px 0;
`;
