import styled from "styled-components";

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: #f7f9fc;
  border-radius: 24px 24px 0 0;
  top: 150px;
  position: relative;
  width: 100%;

  form {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 100%;
    padding: 0 24px;

    select {
      margin: 24px 0;
    }

    .dropdown__container {
      width: 100%;
    }
  }

  h2 {
    font-family: "OpenSansBold", sans-serif;
    font-size: 18px;
    line-height: 24px;
    color: #2e3a59;
    text-transform: uppercase;
    margin-top: 162px;
  }

  button {
    margin-top: 110px;
    margin-bottom: 50px;
  }
`;

export const Avatar = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  top: -120px;
  background-color: #f7f9fc;
  border-radius: 50%;
  border: 4px solid #00d68f;
  width: 100%;
  max-width: 247px;
  height: 247px;

  .photo {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 247px;

    span {
      position: relative;
    }

    img.camera {
      width: 100%;
      max-width: 82px;
      border-radius: 0;
    }

    span.plus {
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      position: absolute;
      bottom: -10px;
      right: -10px;
      background-color: #ff3d71;
      width: 100%;
      max-width: 34px;
      height: 34px;
      border-radius: 50%;
      border: 3px solid #ffffff;

      img {
        width: 100%;
        max-width: 13px;
      }
    }
  }

  img {
    width: 100%;
    border-radius: 50%;
  }

  input.imageAvatar {
    background-color: #fff;
    padding-right: 0;
    position: absolute;
    opacity: 0;
  }

  input.imageAvatar::before {
    content: "Escolha aqui";
    display: none;
  }
`;

export const Inputs = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  margin-top: 162px;

  .input__container {
    font-family: "OpenSansBold", sans-serif;
    width: 100%;
    margin-bottom: 24px;
  }

  &.without-avatar {
    margin-top: 18px;
  }

  .icons {
    span.label-icons {
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: flex-start;
      margin-bottom: 8px;
      text-transform: uppercase;

      img {
        margin-right: 12px;
      }
    }

    label {
      margin: 0;
    }
  }
`;

export const Title = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 24px;

  h1 {
    font-family: "OpenSansSemiBold", sans-serif;
    font-size: 15px;
    line-height: 24px;
    color: #2e3a59;
    text-transform: uppercase;
    margin: 0 12px;
  }

  .hl {
    border-bottom: 1px solid #c5cee0;
    width: 100vw;
    max-width: 125px;
  }

  &.skills,
  &.statistics {
    margin-top: 0;
  }

  .skills-hl,
  .statistics-hl {
    border-bottom: 1px solid #c5cee0;
    width: 100vw;
    max-width: 95px;
  }
`;

export const Statistics = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 22px;
  width: 100%;
  padding: 0 24px;
`;

export const Statistic = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 20px;

  &:last-child {
    margin-bottom: 0;
  }

  span {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;

    img {
      margin-right: 12px;
    }

    p {
      font-family: "OpenSansBold", sans-serif;
      font-size: 12px;
      line-height: 16px;
      text-transform: uppercase;
    }
  }

  p.value {
    font-family: "OpenSansSemiBold", sans-serif;
    font-weight: 600;
    font-size: 15px;
    line-height: 24px;
  }
`;
