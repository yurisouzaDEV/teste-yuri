import React, { useRef, useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import PokeModal from "react-modal";
import * as Yup from "yup";
import { Form } from "@unform/web";

import { overlayStyle, contentStyle } from "./modalStyles";

import Button from "~/components/Button";

import InputText from "~/components/TextInput";
import NumberInput from "~/components/NumberInput";
import Dropdown from "~/components/Dropdown";

import TypesListActions from "~/store/ducks/type-list";

import { Content, Avatar, Inputs, Title } from "./styles";

import closeIcon from "~/assets/images/close.png";
import Shield from "~/assets/images/shield.png";
import Speed from "~/assets/images/speed.png";
import Sword from "~/assets/images/sword.png";
import Camera from "~/assets/images/camera.png";
import Plus from "~/assets/images/plus.png";

PokeModal.setAppElement("#root");

export default function ModalCreate({ openModal, closeModal, data, action }) {
  const dispatch = useDispatch();
  const formRef = useRef();
  const [attach, setAttach] = useState(undefined);
  const [formData, setFormData] = useState({});

  const [typeValue, setTypeValue] = useState({});

  function handleChange(event) {
    setAttach(event.target.files[0]);
  }

  function handleChangeInput(event) {
    const { name, value } = event.target;
    setFormData((state) => ({ ...state, [name]: value }));
  }

  const { data: typesSelectOptions } = useSelector((state) => state.typeList);

  function handleTypeValue(value) {
    setTypeValue(value);
  }

  function handleSelectChange(name, option) {
    const value = option ? option.value : null;
    setFormData((state) => ({ ...state, [name]: value }));
  }

  async function handleSubmit() {
    try {
      formRef.current.setErrors({});

      const schema = Yup.object().shape({
        name: Yup.string().typeError("Obrigatório").required("Obrigatório"),
        hp: Yup.number().required("Obrigatório"),
        peso: Yup.number().required("Obrigatório"),
        altura: Yup.number().required("Obrigatório"),
        hab1: Yup.string().required("Obrigatório"),
        hab2: Yup.string().required("Obrigatório"),
        hab3: Yup.string().required("Obrigatório"),
        hab4: Yup.string().required("Obrigatório"),
        def: Yup.number().required("Obrigatório"),
        atq: Yup.number().required("Obrigatório"),
        atq_esp: Yup.number().required("Obrigatório"),
        def_esp: Yup.number().required("Obrigatório"),
        vel: Yup.number().required("Obrigatório"),
      });

      // await schema.validate(formData, {
      //   abortEarly: false,
      // });

      console.log("Form", formData);
    } catch (err) {
      const validationErrors = {};
      if (err instanceof Yup.ValidationError) {
        err.inner.forEach((error) => {
          validationErrors[error.path] = error.message;
        });
        formRef.current.setErrors(validationErrors);
      }
    }
  }

  const typesSelectFetchCB = useCallback(() => {
    dispatch(TypesListActions.selectRequest());
  }, [dispatch]);

  useEffect(() => {
    typesSelectFetchCB();
  }, [typesSelectFetchCB]);

  return (
    <PokeModal
      className="modalScrollBar"
      isOpen={openModal}
      onRequestClose={closeModal}
      style={{ overlay: overlayStyle, content: contentStyle }}>
      <button className="closeModal" type="button" onClick={closeModal}>
        <img className="modal__close" src={closeIcon} alt="Fechar" />
      </button>
      <Content>
        {formData && (
          <Form ref={formRef} onSubmit={handleSubmit}>
            <Avatar>
              <div className="photo">
                <span>
                  <img className="camera" src={Camera} alt="Camera" />
                  <span className="plus">
                    <img src={Plus} alt="Plus" />
                  </span>
                </span>
              </div>
              <input
                className="imageAvatar"
                type="file"
                name="attach"
                onChange={handleChange}
              />
            </Avatar>
            <Inputs>
              <InputText
                label="NOME"
                placeholder="Nome"
                name="name"
                onChange={handleChangeInput}
                value={formData.name}
                required
              />
              <NumberInput
                label="HP"
                placeholder="HP"
                name="hp"
                onChange={handleChangeInput}
                value={formData.hp}
                required
              />
              <NumberInput
                label="PESO"
                placeholder="Peso"
                name="peso"
                onChange={handleChangeInput}
                value={formData.peso}
                required
              />
              <NumberInput
                label="ALTURA"
                placeholder="Altura"
                name="altura"
                onChange={handleChangeInput}
                value={formData.altura}
                required
              />
            </Inputs>
            <Title>
              <hr className="hl" />
              <h1>tipo</h1>
              <hr className="hl" />
            </Title>
            <Dropdown
              name="selected"
              options={typesSelectOptions}
              onChange={(value) => {
                handleTypeValue(value);
                handleSelectChange("selected", value);
              }}
              value={typeValue}
            />
            <Title className="skills">
              <hr className="skills-hl" />
              <h1>habilidades</h1>
              <hr className="skills-hl" />
            </Title>
            <Inputs className="without-avatar">
              <InputText
                placeholder="Habilidade 1"
                name="hab1"
                onChange={handleChangeInput}
                value={formData.hab1}
                required
              />
              <InputText
                placeholder="Habilidade 2"
                name="hab2"
                onChange={handleChangeInput}
                value={formData.hab2}
                required
              />
              <InputText
                placeholder="Habilidade 3"
                name="hab3"
                onChange={handleChangeInput}
                value={formData.hab3}
                required
              />
              <InputText
                placeholder="Habilidade 4"
                name="hab4"
                onChange={handleChangeInput}
                value={formData.hab4}
                required
              />
            </Inputs>
            <Title className="statistics">
              <hr className="statistics-hl" />
              <h1>estatísticas</h1>
              <hr className="statistics-hl" />
            </Title>
            <Inputs className="without-avatar">
              <NumberInput
                className="icons"
                icon={Shield}
                label="Defesa"
                placeholder="00"
                name="def"
                onChange={handleChangeInput}
                value={formData.def}
                required
              />
              <NumberInput
                className="icons"
                icon={Sword}
                label="Ataque"
                placeholder="00"
                name="atq"
                onChange={handleChangeInput}
                value={formData.atq}
                required
              />
              <NumberInput
                className="icons"
                icon={Shield}
                label="Defesa especial"
                placeholder="00"
                name="def_esp"
                onChange={handleChangeInput}
                value={formData.def_esp}
                required
              />
              <NumberInput
                className="icons"
                icon={Sword}
                label="Ataque especial"
                placeholder="00"
                name="atq_esp"
                onChange={handleChangeInput}
                value={formData.atq_esp}
                required
              />
              <NumberInput
                className="icons"
                icon={Speed}
                label="Velocidade"
                placeholder="00"
                name="vel"
                onChange={handleChangeInput}
                value={formData.vel}
                required
              />
            </Inputs>
            <Button
              type="button"
              onClick={() => handleSubmit()}
              text="CRIAR POKEMON"
            />
          </Form>
        )}
      </Content>
    </PokeModal>
  );
}
