import { combineReducers } from "redux";
import { reducer as pokemon } from "./pokemon";
import { reducer as pokemons } from "./pokemons";
import { reducer as typeList } from "./type-list";

const reducers = combineReducers({
  pokemon,
  pokemons,
  typeList,
});

export default reducers;
