/* eslint-disable no-underscore-dangle */
import { createReducer, createActions } from "reduxsauce";
import update from "immutability-helper";

const { Types, Creators } = createActions(
  {
    listRequest: ["params"],
    listSuccess: ["data"],
    listFailure: ["error"],
    selectRequest: ["params"],
    selectSuccess: ["data"],
    selectFailure: ["error"],
  },
  { prefix: "TYPES_LIST_" }
);

export const TypeListTypes = Types;
export default Creators;

export const INITIAL_STATE = {
  data: [],
  loading: false,
  error: null,
};

// fetch
export const _listRequest = (state) =>
  update(state, {
    loading: { $set: true },
    error: { $set: null },
  });
export const _listSuccess = (state, action) =>
  update(state, {
    loading: { $set: false },
    data: { $set: action.data },
  });
export const _listFailure = (state, action) =>
  update(state, {
    loading: { $set: false },
    error: { $set: action.error },
  });

// select
export const _selectRequest = (state) =>
  update(state, {
    loading: { $set: true },
    error: { $set: null },
  });
export const _selectSuccess = (state, action) =>
  update(state, {
    loading: { $set: false },
    data: { $set: action.data },
  });
export const _selectFailure = (state, action) =>
  update(state, {
    loading: { $set: false },
    error: { $set: action.error },
  });

/* Reducers to types */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LIST_REQUEST]: _listRequest,
  [Types.LIST_SUCCESS]: _listSuccess,
  [Types.LIST_FAILURE]: _listFailure,
  [Types.SELECT_REQUEST]: _selectRequest,
  [Types.SELECT_SUCCESS]: _selectSuccess,
  [Types.SELECT_FAILURE]: _selectFailure,
});
