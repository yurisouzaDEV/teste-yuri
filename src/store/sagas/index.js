import { all, takeLatest } from "redux-saga/effects";

// pokemon
import { PokemonTypes } from "~/store/ducks/pokemon";
import { fetch as fetchPokemon } from "./pokemon";

// pokemons
import { PokemonsTypes } from "~/store/ducks/pokemons";
import { fetch as fetchPokemons } from "./pokemons";

// type-list
import { TypeListTypes } from "../ducks/type-list";
import {
  fetchList as typeListFetch,
  fetchSelect as typeSelectFetch,
} from "./type-list";

export default function* rootSaga() {
  yield all([
    // pokemon
    takeLatest(PokemonTypes.REQUEST, fetchPokemon),

    // pokemons
    takeLatest(PokemonsTypes.REQUEST, fetchPokemons),

    // type-list
    takeLatest(TypeListTypes.LIST_REQUEST, typeListFetch),
    takeLatest(TypeListTypes.SELECT_REQUEST, typeSelectFetch),
  ]);
}
