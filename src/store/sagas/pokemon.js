import { call, put } from "redux-saga/effects";

import api from "~/services/api";
import PokemonActions from "~/store/ducks/pokemon";

export function* fetch(action) {
  try {
    const { params = {} } = action;

    const url = `/pokemon/${params}`;
    const { data } = yield call(api.get, url);

    yield put(PokemonActions.success(data));
  } catch (err) {
    yield put(PokemonActions.failure(err.response.data.error.message));
  }
}
