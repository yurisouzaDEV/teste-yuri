import { call, put } from "redux-saga/effects";

import api, { queryBuilder } from "../../services/api";
import TypeListActions from "../ducks/type-list";

export function* fetchList() {
  try {
    const url = `/type`;
    const { data } = yield call(api.get, url);
    yield put(TypeListActions.listSuccess(data.data));
  } catch (err) {
    yield put(TypeListActions.listFailure(err.response.data.error.message));
  }
}

export function* fetchSelect() {
  try {
    const url = `/type`;

    const { data } = yield call(api.get, url);

    yield put(TypeListActions.selectSuccess(data.results));
  } catch (err) {
    yield put(TypeListActions.selectFailure(err.response.data.error.message));
  }
}
