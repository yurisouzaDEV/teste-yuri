import { call, put } from "redux-saga/effects";

import api, { queryBuilder } from "../../services/api";
import PokemonsActions from "../ducks/pokemons";

export function* fetch(action) {
  try {
    const { params = {} } = action;
    const query = queryBuilder(params);

    const url = `/pokemon?${query}`;
    const { data: responseData } = yield call(api.get, url);
    const { data, ...pagination } = responseData;

    yield put(PokemonsActions.success(data, pagination));
  } catch (err) {
    yield put(PokemonsActions.failure(err.response.data.error.message));
  }
}
