import React, { useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";

import WOW from "wowjs";
import Sidebar from "~/components/Sidebar";
import ModalCapture from "~/components/ModalCapture";

import { Content, Ash } from "./styles";

import AshIMG from "~/assets/images/ashFront.png";
import SearchTooltip from "~/assets/images/searchTooltip.png";
import AshGif from "~/assets/images/ash.gif";
import SearchingTooltip from "~/assets/images/searchingTooltip.png";
import TooltipError from "~/assets/images/tooltipError.png";

import PokemonActions from "~/store/ducks/pokemon";

export default function MapPage() {
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(false);
  const [search, setSearch] = useState();
  const [searching, setSearching] = useState();
  const [run, setRun] = useState(false);
  const [dataPoke, setDataPoke] = useState([]);

  const pokemon = useSelector((state) => state.pokemon.data);
  // const pokemonLoading = useSelector((state) => state.pokemon.loading);

  useEffect(() => {
    const wow = new WOW.WOW({ live: false });
    wow.init();
  }, [search]);

  function getRandomPoke(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  async function handleSearch() {
    try {
      dispatch(PokemonActions.request(getRandomPoke(1, 807)));
    } catch (err) {
      console.log(err);
    }
  }

  function catchPokemon() {
    setDataPoke([...dataPoke, pokemon]);
  }

  const handleClose = useCallback(() => {
    setOpenModal(false);
  }, []);

  if (run === true) {
    setTimeout(() => {
      setRun(false);
    }, 1500);
  }

  return (
    <>
      <div className="map">
        <Sidebar pokes={dataPoke} />
        <Content>
          {run === false ? (
            <Ash
              onMouseEnter={() => setSearch("wow fadeInUp")}
              onMouseLeave={() => setSearch("wow fadeOutDown")}
              onClick={() => {
                if (dataPoke.length < 6) {
                  setSearch("wow fadeOutDown");
                  setRun(true);
                  handleSearch();
                  setTimeout(() => {
                    setOpenModal(true);
                  }, 1200);
                }
              }}>
              {dataPoke.length < 6 ? (
                <img
                  className={search}
                  data-wow-duration="1s"
                  src={SearchTooltip}
                  alt="searchTooltip"
                />
              ) : (
                <img
                  className={search}
                  data-wow-duration="1s"
                  src={TooltipError}
                  alt="searchTooltip"
                />
              )}
              <img className="ash" src={AshIMG} alt="Ash" />
            </Ash>
          ) : (
            <Ash onMouseLeave={() => setSearching("wow fadeOutLeft")}>
              <img
                className={searching}
                data-wow-duration="1.5s"
                src={SearchingTooltip}
                alt="searchingTooltip"
              />
              <img className="ash" src={AshGif} alt="Ash" />
            </Ash>
          )}
        </Content>
        <ModalCapture
          data={pokemon}
          openModal={openModal}
          closeModal={handleClose}
          action={() => {
            catchPokemon();
            handleClose();
          }}
        />
      </div>
    </>
  );
}
