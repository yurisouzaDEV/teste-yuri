import styled from "styled-components";

export const Content = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  width: 100vw;
  height: 100vh;
`;

export const Ash = styled.button.attrs({
  type: "button",
})`
  display: flex;
  flex-direction: column;
  position: relative;
  background-color: transparent;
  border: none;

  img:nth-child(1) {
    display: flex;
    position: absolute;
    bottom: 80px;
    visibility: hidden;

    &.fadeOutDown,
    &.fadeOutLeft {
      visibility: hidden !important;
      transition: 1000ms !important;
    }
  }

  img.tool {
    display: flex;
    position: absolute;
    bottom: 80px;
  }
`;
