import styled from "styled-components";

export const PageContainer = styled.main`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100vh;
  background: linear-gradient(90deg, #43e97b 0%, #38f9d7 100%);

  button {
    margin-top: 32px;
  }
`;
