import React from "react";
import { Link } from "react-router-dom";

import { PageContainer } from "./styles";

import Button from "~/components/Button";

import LogoPokemon from "~/assets/images/pokemonLogo.png";

export default function HomePage() {
  return (
    <PageContainer>
      <img src={LogoPokemon} alt="Logo Pokémon" />
      <Link to="/map">
        <Button text="START" />
      </Link>
    </PageContainer>
  );
}
