import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import Home from "~/screens/Home";
import MapPage from "~/screens/Map";

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/map" exact component={MapPage} />
    </Switch>
  );
}
