import { createGlobalStyle } from "styled-components";

import OpenSansRegular from "~/assets/fonts/Open_Sans/OpenSans-Regular.ttf";
import OpenSansSemiBold from "~/assets/fonts/Open_Sans/OpenSans-SemiBold.ttf";
import OpenSansBold from "~/assets/fonts/Open_Sans/OpenSans-Bold.ttf";

const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: "OpenSansRegular";
    src: url(${OpenSansRegular}) format("opentype");
  }

  @font-face {
    font-family: "OpenSansSemiBold";
    src: url(${OpenSansSemiBold}) format("opentype");
  }

  @font-face {
    font-family: "OpenSansBold";
    src: url(${OpenSansBold}) format("opentype");
  }

  body {
    color: #2E3A59;
  }

  button.pokeball {
    display: flex;
    flex-direction: column;
    position: fixed;
    border: none;
    background-color: transparent;
    width: 100%;
    max-width: 96px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, 150%);

    img {
      width: 100%;
    }
  }

  .modalScrollBar {
    &::-webkit-scrollbar {
      width: 0px;
      background: transparent;
    }
  }

  .sidebar button {
    z-index: 1;
  }

  .sidebar__item img {
    width: 100%;
    max-width: 50px;
  }

  .sidebar__item button {
    width: 100%;
    height: 100%;
    border: none;
    border-radius: 50%;
    z-index: 1;
  }
`;

export default GlobalStyle;
