import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import "~/config/ReactotronConfig";
import { store, persistor } from "~/store";

import history from "~/services/history";
import Routes from "~/routes";

import GlobalStyle from "~/styles/global";

export default function App() {
  return (
    // <Switch>
    //   <Route path="/map" component={MapPage} />
    // </Switch>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter basename="/">
          <GlobalStyle />
          <Routes history={history} />
        </BrowserRouter>
      </PersistGate>
    </Provider>
  );
}
